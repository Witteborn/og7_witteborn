package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends Position {
	// Attribute
	private int maxKapazitaet;
	private String typ;
	private String antrieb;
	private int winkel;
	
	// Methoden
	public Raumschiff() {
		super();
		this.setAntrieb("Undefined");
		this.setMaxKapazitaet(0);
		this.setTyp("Undefined");
		this.setWinkel(0);
	}
	
	/**
	 * @return the maxKapazitaet
	 */
	public int getMaxKapazitaet() {
		return maxKapazitaet;
	}

	/**
	 * @param maxKapazitaet the maxKapazitaet to set
	 */
	public void setMaxKapazitaet(int maxKapazitaet) {
		this.maxKapazitaet = maxKapazitaet;
	}

	/**
	 * @return the typ
	 */
	public String getTyp() {
		return typ;
	}

	/**
	 * @param typ the typ to set
	 */
	public void setTyp(String typ) {
		this.typ = typ;
	}

	/**
	 * @return the antrieb
	 */
	public String getAntrieb() {
		return antrieb;
	}

	/**
	 * @param antrieb the antrieb to set
	 */
	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	/**
	 * @return the winkel
	 */
	public int getWinkel() {
		return winkel;
	}

	/**
	 * @param winkel the winkel to set
	 */
	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
