package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet extends Position {
	// Attribute
	private String name;
	private int anzahlHafen;
	
	// Methoden
	public Planet() {
		super();
		this.setName("Undefined");
		this.setAnzahlHafen(0);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the anzahlHafen
	 */
	public int getAnzahlHafen() {
		return anzahlHafen;
	}

	/**
	 * @param anzahlHafen the anzahlHafen to set
	 */
	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen;
	}
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
