package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author G�khan Witteborn
 * @version 11.2018
 */
public class Pilot extends Position {
	// Attribute
	private String name;
	private String grad;
	
	// Methoden
	public Pilot() {
		super();
		this.setName("Undefined");
		this.setGrad("Undefined");
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the grad
	 */
	public String getGrad() {
		return grad;
	}
	/**
	 * @param grad the grad to set
	 */
	public void setGrad(String grad) {
		this.grad = grad;
	}
}