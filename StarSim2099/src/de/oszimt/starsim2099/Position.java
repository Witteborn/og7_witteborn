package de.oszimt.starsim2099;

public abstract class Position {
	private double posX;
	private double posY;
	
	
	public Position() {
		this.setPosX(0);
		this.setPosY(0);
	}
	
	public Position(int posX, int posY) {
		this.setPosX(posX);
		this.setPosY(posY);
	}
	
	/**
	 * @return the posX
	 */
	public double getPosX() {
		return posX;
	}
	/**
	 * @param posX the posX to set
	 */
	public void setPosX(double posX) {
		this.posX = posX;
	}
	/**
	 * @return the posY
	 */
	public double getPosY() {
		return posY;
	}
	/**
	 * @param posY the posY to set
	 */
	public void setPosY(double posY) {
		this.posY = posY;
	}
}