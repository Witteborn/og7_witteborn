package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author G�khan Witteborn
 * @version 11.2018
 */
public class Ladung extends Position {
	// Attribute
	private int masse;
	private String typ;
	// Methoden
	
	/**
	 */
	public Ladung() {
		super();
		this.setMasse(0);
		this.setTyp("Undefined");
	}
	
	/**
	 * @return the masse
	 */
	public int getMasse() {
		return masse;
	}

	/**
	 * @param masse the masse to set
	 */
	public void setMasse(int masse) {
		this.masse = masse;
	}

	/**
	 * @return the typ
	 */
	public String getTyp() {
		return typ;
	}

	/**
	 * @param typ the typ to set
	 */
	public void setTyp(String typ) {
		this.typ = typ;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}