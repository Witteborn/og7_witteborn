package logic;

import javax.swing.JOptionPane;

public class ErrorHandling {
	public static void alert(Exception e) {
		popUpError(e.getMessage());
		System.out.println("Error: "+e.getMessage());
		//e.printStackTrace();
	}
	
	public static void alert(String text) {
		popUpError(text);
		System.out.println("Error: "+text);
	}

	private static void popUpError(String message) {
		JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.OK_OPTION);
	}
}
