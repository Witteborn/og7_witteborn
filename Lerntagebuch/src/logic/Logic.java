package logic;

import java.util.List;

public class Logic {
	private List<Lerneintrag> ListEntries;
	private String LearnerName;
	private FileControl filecontrol;

	public Logic(FileControl filecontrol) {
		this.filecontrol = filecontrol;
		this.LearnerName = filecontrol.learnerNameEinlesen();
		this.ListEntries = filecontrol.lerneinträgeEinlesen();
	}

	public List<Lerneintrag> getListEntries() {
		return ListEntries;
	}

	public void setListEntries(List<Lerneintrag> listEntries) {
		ListEntries = listEntries;
	}

	public String getLearnerName() {
		return LearnerName;
	}

	public void setLearnerName(String learnerName) {
		LearnerName = learnerName;
	}

	public void reloadFromDisk() {
		this.LearnerName = filecontrol.learnerNameEinlesen();
		this.ListEntries = filecontrol.lerneinträgeEinlesen();
	}

	public void SaveEntryToDisk(Lerneintrag le) {
		filecontrol.addEntry(le);
		reloadFromDisk();
	}

	public String getReport() {
		int sum = 0;

		for (Lerneintrag lerneintrag : ListEntries) {
			sum += lerneintrag.getDauer();
		}

		return  "Anzahl Eintraege: " + ListEntries.size()
				+"\nGesamtdauer der Lernzeit: " + sum
				+ "\nDurchschnittliche Lernzeit: " + sum / ListEntries.size();
	}

}
