package logic;

import java.awt.EventQueue;
import GUI.MeineSuperDuperGUI;

public class Main {
	public static void main(String[] args) {
		OpenLandingPage();
	}

	private static void OpenLandingPage() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MeineSuperDuperGUI frame = new MeineSuperDuperGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					ErrorHandling.alert(e);
				}
			}
		});
	}

}