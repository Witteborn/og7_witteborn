package logic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class FileControl {
	private File file;

	public FileControl(File file) {
		this.file = file;
	}

	public String learnerNameEinlesen() {
		System.out.println("Lese Name aus datei");
		try {
			InputStreamReader reader = new InputStreamReader(new FileInputStream(file), "UTF-8");
			BufferedReader br = new BufferedReader(reader);
			String line = br.readLine();
			br.close();
			return line;
		} catch (FileNotFoundException e) {
			ErrorHandling.alert(e);
		} catch (IOException e) {
			ErrorHandling.alert(e);
		}
		return null;
	}

	public List<Lerneintrag> lerneinträgeEinlesen() {
		System.out.println("Lese alle Lerneinträge aus Datei");
		List<Lerneintrag> eintraege;
		eintraege = new ArrayList<Lerneintrag>();
		try {
			InputStreamReader inStreamReader = new InputStreamReader(new FileInputStream(file), "UTF-8");
			BufferedReader buffReader = new BufferedReader(inStreamReader);
			Lerneintrag lerneintrag;
			String st;
			buffReader.readLine();
			while ((st = buffReader.readLine()) != null) {
				lerneintrag = new Lerneintrag();
				lerneintrag.setDatum(new SimpleDateFormat("dd.MM.yyyy").parse(st));
				lerneintrag.setFach(buffReader.readLine());
				lerneintrag.setBeschreibung(buffReader.readLine());
				lerneintrag.setDauer(Integer.valueOf(buffReader.readLine()));
				//System.out.println(lerneintrag.toString());
				eintraege.add(lerneintrag);
			}
			buffReader.close();
		} catch (Exception e) {
			ErrorHandling.alert(e);
		}

		return eintraege;
	}

	public void addEntry(Lerneintrag eintrag) {
		System.out.println("Speichere neuen Eintrag");
		Writer fileWriter = null;
		try {
			// System.out.println(eintrag.toString());
			fileWriter = new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8");
			fileWriter.write("\n" + new SimpleDateFormat("dd.MM.yyyy").format(eintrag.getDatum()) + "\n");
			fileWriter.write(eintrag.getFach() + "\n");
			fileWriter.write(eintrag.getBeschreibung() + "\n");
			fileWriter.write(eintrag.getDauer() + "");
			fileWriter.close();
		} catch (Exception e) {
			ErrorHandling.alert(e);
		}
	}
}
