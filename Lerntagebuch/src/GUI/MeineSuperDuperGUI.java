package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logic.ErrorHandling;
import logic.FileControl;
import logic.Lerneintrag;
import logic.Logic;

public class MeineSuperDuperGUI extends JFrame {
	private static final long serialVersionUID = -3884166221773329828L;

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	private Logic logic;
	private JTable table;

	public MeineSuperDuperGUI() {
		setTitle("Mein Lerntagebuch");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		final String relativePath = "miriam.dat";
		File file = new File(relativePath);
		FileControl fc = new FileControl(file);
		logic = new Logic(fc);
		JLabel lblLerntagebuchVon = new JLabel("Lerntagebuch von " + logic.getLearnerName());
		lblLerntagebuchVon.setFont(new Font("Tahoma", Font.PLAIN, 21));
		contentPane.add(lblLerntagebuchVon, BorderLayout.NORTH);

		JScrollPane scrollPane = new JScrollPane();

		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane, BorderLayout.CENTER);

		Vector<String> columnNames = new Vector<String>();
		columnNames.addElement("Datum");
		columnNames.addElement("Fach");
		columnNames.addElement("Activitaet");
		columnNames.addElement("Dauer");

		table = new JTable(null, columnNames);

		scrollPane.setViewportView(table);
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(7);
		flowLayout.setHgap(10);
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel, BorderLayout.SOUTH);

		JButton btnNeuerEintrag = new JButton("Neuer Eintrag...");
		btnNeuerEintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							NewEntryFrame frame = new NewEntryFrame(getCurrentObject(), logic);
							frame.setVisible(true);
						} catch (Exception e) {
							ErrorHandling.alert(e);
						}
					}
				});
			}
		});
		panel.add(btnNeuerEintrag);

		JButton btnBericht = new JButton("Bericht...");
		panel.add(btnBericht);
		btnBericht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							ReportFrame frame = new ReportFrame(logic);
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		JButton btnBeenden = new JButton("Beenden");
		panel.add(btnBeenden);
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		updateTable();
	}

	private MeineSuperDuperGUI getCurrentObject() {
		return this;
	}

	public void updateTable() {
		Vector<String> columnNames = new Vector<String>();
		columnNames.addElement("Datum");
		columnNames.addElement("Fach");
		columnNames.addElement("Beschreibung");
		columnNames.addElement("Dauer");
		List<Lerneintrag> eintraege = logic.getListEntries();
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		model.setRowCount(0);
		for (Lerneintrag eintrag : eintraege) {
			String simpleresDatum = dateFormat.format(eintrag.getDatum());
			model.addRow(new Object[] { simpleresDatum, eintrag.getFach(), eintrag.getBeschreibung(), eintrag.getDauer() });
		}
		table.setModel(model);
	}
}
