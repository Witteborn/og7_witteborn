package GUI;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.ErrorHandling;
import logic.Lerneintrag;
import logic.Logic;

public class NewEntryFrame extends JFrame {
	private static final long serialVersionUID = 8503875005796352402L;
	private JPanel contentPane;
	private JTextField textField_Fach;
	private JTextField textField_Datum;
	private JTextField textField_Dauer;
	private JTextField textField_Beschreibung;

	
	
	private void Speichern(Logic logic, MeineSuperDuperGUI mainWindow) {
		Lerneintrag eintrag = new Lerneintrag();
		
		if(
			textField_Beschreibung.getText().isEmpty() 
			|| textField_Datum.getText().isEmpty() 
			|| textField_Dauer.getText().isEmpty() 
			|| textField_Fach.getText().isEmpty()
		   ) 
		   {
			ErrorHandling.alert("Bitte fuelle alle Felder aus");
			return;
		   }
		
		eintrag.setBeschreibung(textField_Beschreibung.getText());
		try {
			eintrag.setDatum(new SimpleDateFormat("dd.MM.yyyy").parse(textField_Datum.getText()));
		eintrag.setDauer(Integer.valueOf(textField_Dauer.getText()));
		} catch (ParseException e1) {
			ErrorHandling.alert("Das Datumsformat ist nicht korrekt, bitte nach dem Schema eintragen: dd.MM.yyyy");
			return;
		}
		catch(NumberFormatException e1) {
			ErrorHandling.alert("Bei der Dauer bitte nur Zahlen eintragen");
			return;
		}
		eintrag.setFach(textField_Fach.getText());
		logic.SaveEntryToDisk(eintrag);		
		mainWindow.updateTable();	
	}
	
	private void Beenden() {
		this.dispose();
	};
	/**
	 * Create the frame.
	 */
	public NewEntryFrame(MeineSuperDuperGUI mainWindow, Logic logic) {
		setTitle("Neuer Eintrag");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 345, 209);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 0, 0);
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 21));
		panel.add(lblNewLabel);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(5, 5, 325, 165);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JLabel lblFach = new JLabel("Fach");
		lblFach.setBounds(10, 30, 45, 15);
		panel_1.add(lblFach);

		textField_Fach = new JTextField();
		textField_Fach.setBounds(10, 45, 85, 20);
		textField_Fach.setBackground(Color.WHITE);
		panel_1.add(textField_Fach);
		textField_Fach.setColumns(1);

		JLabel lblDatum = new JLabel("Datum");
		lblDatum.setBounds(120, 25, 45, 15);
		panel_1.add(lblDatum);

		textField_Datum = new JTextField();
		textField_Datum.setBounds(120, 45, 85, 20);
		textField_Datum.setColumns(1);
		textField_Datum.setBackground(Color.WHITE);
		panel_1.add(textField_Datum);

		JLabel lblDauer = new JLabel("Dauer");
		lblDauer.setBounds(215, 25, 45, 15);
		panel_1.add(lblDauer);

		textField_Dauer = new JTextField();
		textField_Dauer.setBounds(215, 45, 85, 20);
		textField_Dauer.setColumns(1);
		textField_Dauer.setBackground(Color.WHITE);
		panel_1.add(textField_Dauer);

		JLabel lblBeschreibung = new JLabel("Beschreibung");
		lblBeschreibung.setBounds(10, 75, 85, 20);
		panel_1.add(lblBeschreibung);

		textField_Beschreibung = new JTextField();
		textField_Beschreibung.setBounds(10, 100, 300, 20);
		panel_1.add(textField_Beschreibung);
		textField_Beschreibung.setColumns(10);

		JButton btnSpeichern = new JButton("Speichern");
		btnSpeichern.setBounds(10, 130, 100, 20);
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Speichern(logic,mainWindow);
			}

			
		});
			
		panel_1.add(btnSpeichern);
		
		JButton btnSpeichernUndBeenden = new JButton("Speichern und Beenden");
		btnSpeichernUndBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Speichern(logic,mainWindow);
				Beenden();
			}
		});
		btnSpeichernUndBeenden.setBounds(120, 130, 190, 20);
		panel_1.add(btnSpeichernUndBeenden);
	}
}
