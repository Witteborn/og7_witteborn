package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

		boolean pressedBeenden = obj == f.btnBeenden;
		boolean pressedEinfuellen = obj == f.btnEinfuellen;
		boolean pressedVerbrauchen = obj == f.btnVerbrauchen;
		boolean pressedZuruecksetzen = obj == f.btnReset;

		if (pressedBeenden)
			System.exit(0);

		if (pressedEinfuellen) {
			byte fuellstand = f.myTank.getFuellstand();
			fuellstand += 5;
			f.myTank.setFuellstand(fuellstand);
			fuellstand = f.myTank.getFuellstand();
			f.lblFuellstand.setText(fuellstand + "%");
		}

		if (pressedVerbrauchen) {
			byte fuellstand = f.myTank.getFuellstand();
			fuellstand -= 2;
			f.myTank.setFuellstand(fuellstand);
			fuellstand = f.myTank.getFuellstand();
			f.lblFuellstand.setText(fuellstand + "%");
		}

		if (pressedZuruecksetzen) {
			f.myTank.setFuellstand((byte) 0);
			byte fuellstand = f.myTank.getFuellstand();
			f.lblFuellstand.setText(fuellstand + "%");
		}
	}
}