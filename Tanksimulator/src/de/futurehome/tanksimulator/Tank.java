package de.futurehome.tanksimulator;

public class Tank {

	private byte fuellstand;

	public Tank(byte fuellstand) {
		this.fuellstand = getAdjustedFuel(fuellstand);
	}

	public byte getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(byte fuellstand) {
		this.fuellstand = getAdjustedFuel(fuellstand);
	}

	private static byte getAdjustedFuel(byte fuellstand) {

		final byte maxFuel = 100;
		final byte minFuel = 0;

		if (fuellstand < minFuel)
			fuellstand = minFuel;

		if (fuellstand > maxFuel)
			fuellstand = maxFuel;

		return fuellstand;
	}

}
