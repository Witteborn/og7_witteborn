package omnom;

public class Haustier {

	private final byte max = 100;
	private final byte min = 0;
	private int hunger = max;
	private int muede = max;
	private int zufrieden = max;
	private int gesund = max;
	private String name = "Nicht definierter Name!";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param hunger
	 * @param muede
	 * @param zufrieden
	 * @param gesundheit
	 */
	public Haustier(int hunger, int muede, int zufrieden, int gesundheit, String name) {
		super();
		this.hunger = hunger;
		this.muede = muede;
		this.zufrieden = zufrieden;
		this.gesund = gesundheit;
		this.name = name;
	}

	public Haustier(String name) {
		super();
		this.name = name;
		// TODO Auto-generated constructor stub
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {

		if (hunger > max) {
			hunger = max;
		}
		if (hunger < min) {
			hunger = min;
		}
		this.hunger = hunger;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {

		if (muede > max) {
			muede = max;
		}
		if (muede < min) {
			muede = min;
		}
		this.muede = muede;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden > max) {
			zufrieden = max;
		}
		if (zufrieden < min) {
			zufrieden = min;
		}
		this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (gesund > max) {
			gesund = max;
		}
		if (gesund < min) {
			gesund = min;
		}
		this.gesund = gesund;
	}

	public void fuettern(int i) {
		setHunger(getHunger() + i);
	}

	public void heilen() {
		setGesund(max);
	}

	public void schlafen(int i) {
		setMuede(getMuede() + i);
	}

	public void spielen(int i) {
		setZufrieden(getZufrieden() + i);
	}

}
