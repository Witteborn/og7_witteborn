package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

public class BunteRechteckGUI extends JFrame {

	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItemNeuesRechteck;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BunteRechteckGUI frame = new BunteRechteckGUI();
		frame.run();
	}

	protected void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}
	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckGUI() {
		this.brc = new BunteRechteckeController();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0, 0, (int) screenSize.getWidth(), (int) screenSize.getHeight());
		contentPane = new Zeichenflaeche(brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		this.menuBar = new JMenuBar();
		setJMenuBar(this.menuBar);
		this.menu = new JMenu("Hinzufügen");
		this.menuBar.add(menu);
		this.menuItemNeuesRechteck = new JMenuItem("Rechteck hinzufügen");
		this.menu.add(this.menuItemNeuesRechteck);

		this.menuItemNeuesRechteck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				menuItemNeuesRechteck_Clicked();
			}
		});

		setVisible(true);
	}

	protected void menuItemNeuesRechteck_Clicked() {
		this.brc.rechteckHinzufuegen();
	}

}
