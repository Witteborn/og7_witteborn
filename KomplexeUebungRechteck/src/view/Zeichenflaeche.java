package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import javax.swing.JPanel;
import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel {

	private final BunteRechteckeController brc;

	public Zeichenflaeche(BunteRechteckeController brc) {
		this.brc = brc;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);

		List<Rechteck> rechtecke = brc.getRechtecke();
		for (Rechteck rechteck : rechtecke) {
			g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
		}
	}
}