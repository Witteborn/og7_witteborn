package controller;

import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.RechteckEingabeMaske;

public class BunteRechteckeController {

	public static void main(String[] args) {
		System.out.println("Nope");
	}
	
	private List<Rechteck> rechtecke;
	private MySQLDatabase database;

	public BunteRechteckeController() {	
		this.database = new MySQLDatabase();
		this.rechtecke = this.database.getAlleRechtecke();
	}

	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public boolean add(Rechteck rechteck) {
		
		if (this.getRechtecke() == null) {
			return false;
		}
		
		this.getRechtecke().add(rechteck);
		this.database.rechteck_eintragen(rechteck);
		
		return true;
	}

	public boolean reset() {

		if (rechtecke == null) {
			return false;
		}

		rechtecke.clear();

		if (rechtecke.size() != 0) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();

		for (int i = 0; i < anzahl; i++) {
			rechtecke.add(Rechteck.generiereZufallsRechteck());
		}
	}

	public void rechteckHinzufuegen() {
		new RechteckEingabeMaske(this);		
	}

}
