/*
 * Aufgabe:
 * Erstellen sie im package test die Klasse RechteckTest.
 *  Diese soll 5 Rechtecke mit dem parameterlosen Konstruktor und 5 Rechtecke mit 
 *  dem vollparametrisierten Konstuktor erstellen.
 *  
 * Rechteck 0: 10,10,30,40
 *  Rechteck 1: 25,25,100,20
 *  Rechteck 2: 260,10,200,100
 *  Rechteck 3: 5,500,300,25
 *  Rechteck 4: 100,100,100,100
 *  Rechteck 5: 200,200,200,200
 *  Rechteck 6: 800,400,20,20
 *  Rechteck 7: 800,450,20,20
 *  Rechteck 8: 850,400,20,20
 *  Rechteck 9: 855,455,25,25
*/

package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {

		// Wieso sollten wir mehr als eines erstellen?
		new Rechteck();
		new Rechteck();
		new Rechteck();
		new Rechteck();
		new Rechteck();

		/*
		 * Erstellen Sie in der Klasse Rechteck die toString Methode, welche f�r das
		 * Rechteck 0 folgende Zeichenkette ausgeben soll:
		 * "Rechteck [x=10, y=10, breite=30, hoehe=40]". Versehen Sie die Methode mit
		 * einer @Override Annotation(Warum?:
		 * https://docs.oracle.com/javase/tutorial/java/annotations/predefined.html).
		 * Testen Sie in der RechteckTest-Klasse mittels Equals, ob Ihre Methode die
		 * richtige Zeichenkette auf der Konsole ausgibt.
		 */

		BunteRechteckeController brc = new BunteRechteckeController();

		brc.add(new Rechteck(10, 10, 30, 40));
		brc.add(new Rechteck(25, 25, 100, 20));
		brc.add(new Rechteck(260, 10, 200, 100));
		brc.add(new Rechteck(5, 500, 300, 25));
		brc.add(new Rechteck(100, 100, 100, 100));
		brc.add(new Rechteck(200, 200, 200, 200));
		brc.add(new Rechteck(800, 400, 20, 20));
		brc.add(new Rechteck(800, 450, 20, 20));
		brc.add(new Rechteck(850, 400, 20, 20));
		brc.add(new Rechteck(855, 455, 25, 25));

		final String erwarteteAusgabe = "Rechteck [x=10, y=10, breite=30, hoehe=40]";
		final String erhalteneAusgabe = brc.getRechtecke().get(0).toString();

		if (!erhalteneAusgabe.equals(erwarteteAusgabe)) {
			System.out.println("Ein Fehler ist aufgetreten, Rechteck.toString() gibt nicht die richtigen Werte aus");
			System.out.println("Erwartete Ausgabe: " + erwarteteAusgabe + "\nErhaltene Ausgabe: " + erhalteneAusgabe);
		}

		final String erwarteteAusgabeListe = "BunteRechteckeController [" + "rechtecke=["
				+ "Rechteck [x=10, y=10, breite=30, hoehe=40], " + "Rechteck [x=25, y=25, breite=100, hoehe=20], "
				+ "Rechteck [x=260, y=10, breite=200, hoehe=100], " + "Rechteck [x=5, y=500, breite=300, hoehe=25], "
				+ "Rechteck [x=100, y=100, breite=100, hoehe=100], "
				+ "Rechteck [x=200, y=200, breite=200, hoehe=200], " + "Rechteck [x=800, y=400, breite=20, hoehe=20], "
				+ "Rechteck [x=800, y=450, breite=20, hoehe=20], " + "Rechteck [x=850, y=400, breite=20, hoehe=20], "
				+ "Rechteck [x=855, y=455, breite=25, hoehe=25]" + "]" + "]";

		System.out.println(brc.toString());

		if (!erwarteteAusgabeListe.equals(brc.toString())) {
			System.out.println(
					"Ein Fehler ist aufgetreten, BunteRechteckListe.toString() gibt nicht die richtigen Werte aus");
			System.out
					.println("Erwartete Ausgabe: " + erwarteteAusgabeListe + "\nErhaltene Ausgabe: " + brc.toString());
		}

		// Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist
		Rechteck eck10 = new Rechteck(-4, -5, -50, -200);
		System.out.println(eck10); // Rechteck [x=-4, y=-5, breite=50, hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);// Rechteck [x=-10, y=-10, breite=200, hoehe=100]

		rechteckeTesten();

		System.out.println("RechteckTest: Alle Tests wurden durchlaufen!");
	}

	private static void rechteckeTesten() {
		boolean test = true;
		Rechteck aussenRand = new Rechteck(0, 0, 1200, 1000);
		for (int i = 0; i < 50_000; i++) {
			Rechteck r = Rechteck.generiereZufallsRechteck();
			if (!aussenRand.enthaelt(r)) {
				System.out.println(r);
				test = false;
			}
		}

		System.out.println("Rechteckgenerierungstest bestanden: " + test);

	}

}